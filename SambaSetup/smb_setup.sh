#!/bin/bash
echo ===================================
echo Samba instalation and configuration
echo ===================================

sleep 1
sudo apt update 
sudo apt upgrade

sudo apt install samba
sudo apt install samba samba-common-bin

sudo cp -f ./smb.conf /etc/samba/smb.conf 

echo =================================================================================================
echo You will be asked to enter an username, please use the same as you pi. Take the password you want
echo =================================================================================================

sudo smbpasswd -a pi


sudo /etc/init.d/samba restart
echo =======================================
echo =======================================
echo Your NAS is ready to be used. Have fun!
echo =======================================

